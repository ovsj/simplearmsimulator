#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>

using namespace std;

/* Referência: https://www.safaribooksonline.com/library/view/c-cookbook/0596007612/ch04s07.html
 */
void split(const string& s, char c,
           vector<string>& v) {
   string::size_type i = 0;
   string::size_type j = s.find(c);

   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);

      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

int main()
{
	string line, e; 
	vector<string> v;
	ifstream inFile ("arm_gripper.csv");
	//ofstream outFile1 ("output_a_test.txt");
	//ofstream outFile2 ("output_b_test.txt");
	ofstream outFile1 ("output_a.txt");
	ofstream outFile2 ("output_b.txt");
	
	if (inFile.is_open())
	{
		getline (inFile,line);
		cout << "Cabeçalho: " << line << endl; 
		while ( getline (inFile,line) )
		{
			cout << line << endl;
			split(line,',',v); 
			outFile1 << v.at(0) << " 1:"<<v.at(2)<<" 2:"<<v.at(3) << endl;
			outFile2 << v.at(1) << " 1:"<<v.at(2)<<" 2:"<<v.at(3) << endl;  
			v.clear(); 
		}
		inFile.close();
	}
	outFile1.close();
	/*
	while (inFile.good()) {
		inFile >> d; 
		cout << d << " "<< endl; ;
		cont++; 
		if (cont == 4) {
			cont = 0; 
			cout << endl; 
		}
	}
	inFile.close(); 
	
	
	ofstream outFile1 ("example_output.txt");
	if (outFile1.is_open())
	{
		outFile1 << "This is a line.\n";
		outFile1 << "This is another line.\n";
		outFile1.close();
	}
*/
}
