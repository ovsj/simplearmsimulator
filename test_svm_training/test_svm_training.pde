  
  
PrintWriter output;

void setup() {
  size(510,510);
  // Create a new file in the sketch directory
   
  output = createWriter("/tmp/out_a.txt");
  output.close(); 
  output = createWriter("/tmp/out_b.txt");
  output.close(); 
  frameRate(10); 
}

void draw() {
  if (mousePressed) {
    output = createWriter("input.txt");
    point(mouseX, mouseY);
    ellipse(mouseX,mouseY,10,10); 
    output.println("0 1:"+mouseX + "  2:" + mouseY); // Write the coordinate to the file
    output.flush(); // Writes the remaining data to the file
    output.close(); // Finishes the file    
     
  }
  // obtem uma resposta da svm em um arquivo 
  execComands();
  // ler a resposta para a junta alpha  
  String lines[] = loadStrings("/tmp/out_a.txt");
  print("there are " + lines.length + " lines in a: ");
  for (int i = 0 ; i < lines.length; i++) {
    println(lines[i]+" ");
  }
  // ler a resposta para a junta alpha  
  lines = loadStrings("/tmp/out_b.txt");
  print("there are " + lines.length + " lines in b: ");
  for (int i = 0 ; i < lines.length; i++) {
    println(lines[i]+" ");
  }


}

void execComands() {
  String pathPredict = "/home/oriva/Repositorios/sim_arm/test_svm_training/";
  try {
    Process qq = Runtime.getRuntime().exec(pathPredict+"predict.sh");
    qq.waitFor();
  }
  catch (Exception e) {
     System.err.println("An Exception was caught :"+e.getMessage());
  }
  
  /*
  try {
  ProcessBuilder pb = new ProcessBuilder("myshellScript.sh", "myArg1", "myArg2");
 
  pb.directory(new File("myDir"));
  Process p = pb.start();
  } catch (Throwable t) {
    t.printStackTrace();
  }*/
  /*
  try {
    String target = new String("/home/hagrawal/test.sh");
    Runtime rt = Runtime.getRuntime();
    Process proc = rt.exec(target);
    proc.waitFor();
    StringBuffer output = new StringBuffer();
    BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
    String line = "";                       
    while ((line = reader.readLine())!= null) {
          output.append(line + "\n");
    }
    System.out.println("### " + output);
  } catch (Throwable t) {
    t.printStackTrace();
  }
  */
}

void keyPressed() {
  if ( key == 'x' || key == 'X') {

    exit(); // Stops the program
  }
}
  