int xFrame; 
int yFrame; 
float buttonHeight;
int frameSize; 
float betaStep, alphaStep; 
float inc;
boolean stopGenerating = false; 
boolean toGenerateTraining = false; 

Arm arm;
Button buttonG; 
Button buttonT; 

int mirrorY; 

void setup() {
  frameRate(20); 
  size(430, 520);
  xFrame = 10; 
  yFrame = 10; 
  buttonHeight = 30; 
  frameSize = 400;
  mirrorY = frameSize; 
  
  //inc = PI/13; // for testing
  inc = PI/20; // for training
  betaStep = - PI;
  alphaStep = PI/10; 
  arm = new Arm(50, mirrorY - 30);
  // Button for active the Generation of training samples
  buttonG = new Button("Training Samples", 20, frameSize + 30, frameSize/2 - 20, buttonHeight);  
  // Button for active the test of training
  buttonT = new Button("Test Traning",(frameSize/2) +20, frameSize + 30, frameSize/2 - 20, buttonHeight);
  
}

void draw(){
  background(230);
  stroke(0,125,255);
  fill(255);
  rect(xFrame, yFrame, frameSize, frameSize,10);
  
  arm.Draw(alphaStep,betaStep);
  if (toGenerateTraining)
    gereratingSamples();
  
  arm.DrawPoints(); 
  
  fill(255);
  stroke(0,125,255);
  rect(xFrame, frameSize + 20, frameSize, 85);
 
  buttonG.Draw(); 
  buttonT.Draw();
}

void gereratingSamples() {
  // gerando amostras 
  if ( ! stopGenerating) {
    betaStep = betaStep + inc;
    arm.SaveState(); 
  }
  if (betaStep > 0 && ! stopGenerating) {
    betaStep = -PI;
    alphaStep = alphaStep + inc;
    if (alphaStep > PI/5 + 2*(PI/3))
      stopGenerating = true; 
      //alphaStep = 0; 
  } 
}

void mouseClicked() {
  if (buttonG.MouseIsOver()) {
     println("click");  
     buttonG.setActive( ! buttonG.getActive() ); 
     toGenerateTraining = buttonG.getActive(); 
  }
  
}