class Arm {
  int linkA; 
  int linkB; 
  int sizeNode; 
  
  float alphaJoint;
  float betaJoint;
  
  int x1,x2;
  int y1,y2; 
 
  int xBase;
  int yBase;
  
  Table table; // guarda as amostras (alpha,beta,x2,y2)
  String fileNameDS; 
  
  Arm(int xB, int yB) {
    xBase = xB;
    yBase = yB;
    linkA = 170; 
    linkB = 160; 
    sizeNode = 10;

    table = new Table(); 
    table.addColumn("alpha");
    table.addColumn("beta");
    table.addColumn("x2");
    table.addColumn("y2");
    
    fileNameDS = "data/arm_gripper.csv";
    
    saveTable(table, fileNameDS);
  }
  
  void Draw(float aj, float bj) {
    alphaJoint = aj;   
    betaJoint = bj; 
    x1 = xBase + int(linkA*cos(alphaJoint)); 
    y1 = yBase - int(linkA*sin(alphaJoint));
    
    x2 = x1 + int(linkB*cos(alphaJoint+betaJoint));
    y2 = y1 - int(linkB*sin(alphaJoint+betaJoint));
    
    line(xBase,yBase,x1,y1);
    line(x1,y1,x2,y2);
    
    ellipse(xBase, yBase, sizeNode, sizeNode); 
    ellipse(x1, y1, sizeNode, sizeNode);
    ellipse(x2, y2, sizeNode, sizeNode); 
  }
  
  void SaveState(){
    TableRow newRow = table.addRow();
    newRow.setFloat("alpha", alphaJoint);
    newRow.setFloat("beta", betaJoint);
    newRow.setInt("x2", x2);
    newRow.setInt("y2", y2);
    saveTable(table, fileNameDS);
  }
  
  void DrawPoints() {
    for (TableRow row : table.rows()) {
      
      int x2 = row.getInt("x2");
      int y2 = row.getInt("y2");
      
      stroke(255,125,0);
      ellipse(x2,y2,10,10);
    }
  }
  
  
  
}