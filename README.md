# Controle Inteligente do Braço Robótico 
Este pequeno projeto tem o objetivo de criar um software para o controle inteligente do braço robótico. O foco inicial é no uso do modelo Support Vector Machine (SVM)[1] para controlar as posições do braço robótico. O objetivo final é utilizar uma modelo de aprendizagem não supervisionada para que o sistema robótico aprenda sozinho sobre o seu controle de posicionamento. 

## Pastas 

### sim_arm

Código simples em processing para gerar a base de dados de treinamento e fazer o primeiro teste de integração entre controle e treinamento da SVM. Este código gera um arquivo SVM contendo os dados dos ângulos do braço e a posição final do efetuador

### convert_data_files

Contêm um programa para gerar os arquivos de treinamento do braço robótico para o formato libsvm a partir do formato csv. 

### libsvm_training 

Arquivos necessários para gerar e testar o treinamento da rede. O svm-train é o executável para treinar. O svm-predict é para obter uma resposta da SVM para um dada entrada. 

### best_training

Os arquivos do melhor treinamento encontrado até o momento.

### logs

Descrições dos comandos usados para treinamentos da SVM

### test_svm_training 

Programa simples em processing para testar a integração entre o script que executa a SVM e a interface.
 
## Referência 

[1] Site oficial da libsvm, http://www.csie.ntu.edu.tw/~cjlin/libsvm/ 
